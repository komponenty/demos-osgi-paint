Program pokazujący dynamiczne możliwości OSGi.
W celu użycia należy wydać komendę `gradle console`.
Po uruchomieniu pokaże się okienko aplikacji z 3 kształtami do rysowania.
Po narysowaniu dany kształ można unieruchomić za pomocą odpowiednich komend w konsoli.

Lista dostępnych komend w konsoli jest dostępna po wpisaniu `help`.

Branche
-------

* master - standardowa implementacja wykorzystująca niskopoziome API
* DeclarativeServices - implementacja komponentowa z wykorzystaniem serwisów deklaratywnych