package pk.demos.osgi.paint;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;

import javax.swing.JComponent;

import pk.demos.osgi.shape.SimpleShape;

/**
 * Prosty komponent graficzny reprezentujący rysowany kształt. 
 * Ten komponent używa serwisu <tt>SimpleShape</tt> do narysowania samego siebie.
 **/
public class ShapeComponent extends JComponent {
	private static final long serialVersionUID = 1L;
	private PaintFrame frame;
	private String shapeName;

	/**
	 * Tworzy komponent dla odpowiedniego okienka i o zadanej nazwie kształtu.
	 * 
	 * @param frame The drawing frame associated with the component.
	 * @param shapeName The name of the shape to draw.
	 **/
	public ShapeComponent(PaintFrame frame, String shapeName) {
		this.frame = frame;
		this.shapeName = shapeName;
	}

	/**
	 * Ryzuje zawartość komponentu. Komponent pobiera kształt o danej nazwie
	 * z okienka do rysowania, co pozwala na zachowanie dynamizmu.
	 * 
	 * @param g The graphics object to use for painting.
	 **/
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		SimpleShape shape = frame.getShape(shapeName);
		shape.draw(g2, new Point(getWidth() / 2, getHeight() / 2));
	}
}

