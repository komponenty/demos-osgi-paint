package pk.demos.osgi.paint.internal;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;

import pk.demos.osgi.paint.PaintFrame;

/**
 * Aktywator paczki hostującej. Tworzy główne okienko aplikacji typu <tt>JFrame</tt> 
 * i zaczyna śledzenie serwisu <tt>SimpleShape</tt>.
 * Wszystkie czynności wykonywane są w wątku obsługi zdarzeń Swing-a
 * aby ominąć synchronizację i problemy z odświeżaniem. 
 * Zamknięcie okienka aplikacji powoduje wywołanie <tt>Bundle.stop()</tt> 
 * co prowadzi do zamknięcia JVM przez framework i zakończenie pracy.
 **/

public class Activator implements BundleActivator, Runnable {

	private static BundleContext context;
	private static PaintFrame frame = null;
	private static ShapeTracker shapetracker = null;

	static BundleContext getContext() {
		return context;
	}

	public void start(BundleContext bundleContext) throws Exception {
		Activator.context = bundleContext;
		if (SwingUtilities.isEventDispatchThread()) {
			run();
		} else {
			try {
			    javax.swing.SwingUtilities.invokeLater(this);
			} catch (Exception ex) {
				System.err.println("Nie udało się wystartować");
			    ex.printStackTrace();
			}
		}
	}

	public void stop(BundleContext bundleContext) throws Exception {
		shapetracker.close();
	    final PaintFrame pframe = frame;
	    javax.swing.SwingUtilities.invokeLater(new Runnable() {
	    	public void run() {
	    		pframe.setVisible(false);
	    		pframe.dispose();
	    	}
	    });
		Activator.context = null;
	}

	@Override
	public void run() {
		System.out.println("Startujemy...");
		frame = new PaintFrame();

	    frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	    frame.addWindowListener(new WindowAdapter() {
	    	public void windowClosing(WindowEvent evt) {
	    		try {
	    			// id == 0 ma framework OSGi
	    			context.getBundle(0).stop();
	    		} catch (BundleException ex) {
	    			ex.printStackTrace();
	    		}
	    	}
	    });

	    frame.setVisible(true);

	    shapetracker = new ShapeTracker(context, frame);
	    shapetracker.open();
	    System.out.println("Uff, udało się!");
	}
}
